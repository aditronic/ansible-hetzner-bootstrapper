{{ $lb_tag := key "nginx_lb/loadbalancer_tag" -}}
{{ range services -}}
  {{ if .Tags | contains $lb_tag -}}
    {{- $service_name := .Name -}}
    {{ $backend_service := printf "%v.%v" "backend" $service_name -}}
    {{ $lb_service := printf "%v.%v" $lb_tag $service_name -}}
    {{ $default_domain := key "nginx_lb/default_domain" -}}
    {{ $default_domain_internal := key "nginx_lb/default_domain_internal" -}}
    {{ $cert_default := printf "%v/%v.%v" "/etc/pki/nginx" $service_name "pem" }}
    {{ $cert_key_default := printf "%v/%v.%v" "/etc/pki/nginx/private"
       $service_name "key" }}
    {{ range service $backend_service -}}
      {{ with .ServiceTaggedAddresses.local -}}
        {{ $address := printf "%v:%v" .Address .Port -}}
        {{ scratch.MapSet $backend_service $address $address -}}
      {{- end -}}
    {{- end -}}
    {{ range service $lb_service -}}
      {{ $cert := or .ServiceMeta.cert $cert_default -}}
      {{ scratch.Set "cert" $cert -}}
      {{ $cert_key := or .ServiceMeta.cert_key $cert_key_default -}}
      {{ scratch.Set "cert_key" $cert_key -}}
      {{ $fqdn_default := or .ServiceMeta.fqdn $default_domain }}
      {{ $fqdn_default_internal := or .ServiceMeta.fqdn $default_domain_internal }}
      {{- with .ServiceTaggedAddresses.lan_ipv4 -}}
        {{ $address := printf "%v:%v" .Address .Port -}}
        {{ if ne .Port 0 -}}
          {{ scratch.MapSet $lb_service $address $fqdn_default_internal -}}
        {{ end -}}
      {{ end -}}
      {{- with .ServiceTaggedAddresses.wan_ipv4 -}}
        {{ $address := printf "%v:%v" .Address .Port -}}
        {{ if ne .Port 0 -}}
          {{ scratch.MapSet $lb_service $address $fqdn_default -}}
        {{ end -}}
      {{ end -}}
    {{- end }}
    {{ if scratch.Key $backend_service }}
upstream {{ key "nginx_lb/backend_prefix" }}_{{ $service_name }} {
      {{ range scratch.MapValues $backend_service -}}
{{- "" }}server {{ . }};
      {{- end }}
{{ "}" }}

server {
    {{- range $a,$l := scratch.Get $lb_service -}}
    {{- $service_fqdn := printf "%v.%v" $service_name $l }}
{{ "    " }}listen {{ $a }} ssl;
{{ "    " }}server_name {{ $service_fqdn }};

{{ "    " }}ssl_certificate {{ scratch.Get "cert" }};
{{ "    " }}ssl_certificate_key {{ scratch.Get "cert_key" }};

{{ "    " }}location / {
    {{ range $k,$v := tree "lb/proxy_set_headers" | explode -}}
{{ "    " }}proxy_set_header {{ $k }} {{ $v }};
    {{ end }}
{{ "        " }}proxy_pass http://{{ key "nginx_lb/backend_prefix" }}_{{ $service_name }};
    }
}
    {{ end }}
    {{ end }}
  {{- end -}}
{{ end }}
